package JUnitTest;
import org.junit.*;
import org.junit.Test;
import JUnit.JUnitMethods;

public class JUnitTestString 
{	//indicar para o JUnit que este � um teste case
	//deve ser invocado quando o JUnit for executado
	//deve ser anotar este m�todo com:
	@Test//m�todo de testes p/ o m�todo isEmpty
	public void test_isEmpty()
	{	//vari�vel booleana que ir� receber o retorno
		//do m�todo
		boolean b;
		//invocando o m�todo para testar o retorno de 
		//test_isEmpty(). No momento, esperamos que retorne
		//valor falso
		b = JUnitMethods.isEmpity("abc");
		//m�todo que nos garantir� que b � falso. Sen�o, o
		//JUnit ir� nos avisar que este teste falhou.
		Assert.assertFalse(b);
		
		//mais uma chamada, passando como par�mentro um nulo.
		b = JUnitMethods.isEmpity(null);
		//podemos com a implementa��o do m�todo, converncionar que
		//com um valor nulo, este m�todo isEmpty seja verdadeiro.
		Assert.assertTrue(b);
		
		//ultima chamada, passando uma string vazia
		b = JUnitMethods.isEmpity("");
		Assert.assertTrue(b);
	}
	//teste do m�todo reverse, que inverte o texto da string
	@Test public void teste_reverse()
	{	//variavel string que recebe o inverso do texto abcd
		String r = JUnitMethods.reverse("abcd");
		//m�todo de compara��o, com dois par�metros:
		//1-O resultado esperado;
		//2-valor real
		Assert.assertEquals("dcba", r);
		
		//outra possibilidade
		r = JUnitMethods.reverse(null);
		//garantir que o resultado tbm seja nulo
		Assert.assertEquals(null, r);
	}
	//antes do teste
	@Before public void before()
	{System.out.println("Iniciando");}
	
	//ap�s o teste
	@After public void after()
	{System.out.println("Fim do teste");}
}
