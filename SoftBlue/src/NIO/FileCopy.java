package NIO;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
public class FileCopy 
{	public static void main(String[] args) throws Exception
	{	//inicializando um FileInputStream
		FileInputStream fis = null;
		//inicializando um FileChannel de entrada
		FileChannel fci = null;
		//inicializando FileOutputStream
		FileOutputStream fos = null;
		FileChannel fco = null;
		try
		{	//instanciando um FileInputStream, com o arquivo 
			//salvo
			fis = new FileInputStream("in.txt");
			//definindo um FileChannel que ir� ler as 
			//informa��es do in.txt este serve para retornar um 
			//canal que representa aquele arquivo que est� 
			//associado ao FileInputStream
			fci = fis.getChannel();
			//instanciando um FileOutputStream, para o arquivo 
			//out.txt
			fos = new FileOutputStream("out.txt");
			//definindo FileChannel que ir� escrever o conte�do 
			//de in.txt para o out.txt
			fco = fos.getChannel();
			//instanciado um buffer tipo byte, atrav�s de um m�todo
			//da classe ByteBuffer, que carrega como par�metro um
			//valor em byte, cujo valor ir� depender da aplica��o
			//do usu�rio.
			ByteBuffer buffer = ByteBuffer.allocate(5);
			//iniciando a c�pia
			while(true)
			{	//fazendo a leitura do canal de entrada, passando
				//para o buffer os dados que ser�o lidos. A API
				//ir� fazer automaticamente a leitura apenas da
				//quantidade de dados necess�ria para n�o 
				//extrapolar a quantidade de dados do buffer.
				//� atrav�s do retorno desses bytes lidos que
				//saberei se os dados acabaram
				int bytesRead = fci.read(buffer);
				//condi��o de parada
				if(bytesRead < 0)	break;
				//mudando o buffer do modo de escrita para
				//leitura, pois por default, todo buffer �
				//inicialmente de escrita.
				buffer.flip();
				//fazendo a escrita
				fco.write(buffer);
				//chamando o metodo clear para limpar o
				//buffer e faze-lo voltar para o modo de 
				//escrita
				buffer.clear();
			}
		}
		//devemos fechar os recursos quando terminam de serem 
		//utilizados
		finally
		{	if(fci != null)	fci.close();
			if(fis != null)	fis.close();
			if(fco != null)	fco.close();
			if(fos != null)	fos.close();
		}
	}
}