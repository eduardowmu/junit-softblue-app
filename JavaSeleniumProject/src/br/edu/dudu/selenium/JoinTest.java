package br.edu.dudu.selenium;

import java.io.File;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;

public class JoinTest 
{	public static void main(String[] args) 
	{	//atributos de classe
		String name = "Anonimoua";
		String surname = "Anonimous";
		String celphone = "99999999999";
		String password = "!Qw23456";
		int day = 12;
		int mon = 12;
		int year = 15;
		int sex = 2;
		
	
		//1 - Open the web browser;
		//1.1 - Nome do driver, path (endere�o do diret�rio do driver + 
		System.setProperty("webdriver.gecko.driver",
			//o nome do arquivo do driver)
			"C:\\webdrivers\\geckodriver.exe");
		//String path = "C:\\webdrivers\\chromedriver.exe";
		
		//System.setProperty("webdriver.chrome.driver", path);
		
		//WebDriver driver = new ChromeDriver();
		
		WebDriver driver = new FirefoxDriver(); 
		
		
		//2 - Navigate to the web browser app
		//https://pt-br.facebook.com/
		//se o driver n�o estiver nulo
		if(!driver.equals(null))//entra na p�gina desejada
		{	driver.get("https://pt-br.facebook.com/");
			
			//localiza o <input/> de submiss�o
			//driver.findElement(By.linkText("Inscreva-se")).click();
		
			//uma outra maneira de inserir valor em um <input type="text"/>
			driver.findElement(By.name("firstname")).sendKeys(name);
			
			//inserindo valor no campo Nova Senha
			driver.findElement(By.name("lastname")).sendKeys(surname);
			
			//inserindo numero de telefone
			driver.findElement(By.name("reg_email__")).sendKeys(celphone);
			
			//inserindo senha
			driver.findElement(By.name("reg_passwd__")).sendKeys(password);
			
			driver.findElement(By.cssSelector("input[name='sex'][value='2']")).click();
			
			//selecionando um item do listItem
			//new Select(driver.findElement(By.name("birthday_day"))).selectByIndex(24);
			new Select(driver.findElement(By.name("birthday_day"))).selectByValue(String.valueOf(day));
			new Select(driver.findElement(By.name("birthday_month"))).selectByValue(String.valueOf(mon));
			new Select(driver.findElement(By.name("birthday_year"))).selectByValue(String.valueOf(year));
			
			//selecionando o tipo de sexo
			//driver.findElement(By.id("u_0_9")).click();
			//ou podemos fazer de outra forma
			//driver.findElement(By.cssSelector("input[name='sex'][value='2']")).click();
		}
	}
}