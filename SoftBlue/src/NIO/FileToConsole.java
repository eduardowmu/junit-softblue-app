package NIO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileToConsole 
{	public static void main(String[] args) throws Exception
	{	//inicializando um FileInputStream
		FileInputStream fis = null;
		//inicializando um FileChannel de entrada
		FileChannel fci = null;
		try
		{	//instanciando um FileInputStream, com o  
			//arquivo salvo
			fis = new FileInputStream("in.txt");
			//definindo um FileChannel que ir� ler as 
			//informa��es do in.txt este serve para 
			//retornar um canal que representa aquele 
			//arquivo que est� associado ao
			//FileInputStream
			fci = fis.getChannel();
			ByteBuffer buffer = ByteBuffer.allocate(5);
			//armazenador de dados lidos do buffer
			StringBuilder sb = new StringBuilder();
			//leitura dos dados direto do buffer, pois
			//n�o se deve ler ou escrever dados de um
			//canal
			while(true)
			{	//fazendo a leitura do canal de entrada, 
				//passando para o buffer os dados que ser�o 
				//lidos. A API ir� fazer automaticamente a 
				//leitura apenas da quantidade de dados  
				//necess�ria para n�o extrapolar a quantidade 
				//de dados do buffer. � atrav�s do retorno 
				//desses bytes lidos que saberei se os dados
				//acabaram
				int bytesRead = fci.read(buffer);
				//condi��o de parada, se o numero de butes
				//lidos for menor que 0, para de fazer a
				//leitura
				if(bytesRead < 0)	break;
				//mudando o buffer do modo de escrita para
				//leitura
				buffer.flip();
				//Array de bytes que ser� armazenado no buffer
				//portanto a quantidade � a que est� dispon�vel
				//no buffer. O m�todo indexado retorna o numero
				//de dados, no caso em byte, dentro do buffer
				byte[] bytes = new byte[buffer.remaining()];
				//pegando dados do buffer, de dados tip�
				//bytes
				buffer.get(bytes);
				//armazenando dentro de uma string os dados
				//do buffer, que s�o convertidos em String
				String s = new String(bytes);
				//realizando a concatena��o de strings
				sb.append(s);
				//voltando buffer para o modo original
				buffer.clear();
			}
			//mostrando todo o conte�do do buffer
			System.out.println(sb);
		}
		//devemos fechar os recursos quando terminam  
		//de serem utilizados
		finally
		{	if(fci != null)	fci.close();
			if(fis != null)	fis.close();
		}
	}

}
