package out.production.test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
public class Gerenciamento_SAC implements IEntidade
{	WebDriver obj = new ChromeDriver();

	public void RobotGSAC(massa m)
	{	obj.get("https://release.movida.com.br/QA-AT/login.php?logout=1");
	    //obj.get("https://release.movida.com.br/QA-AT/index.php");
	    //Login
	    obj.findElement(By.name("logon")).sendKeys("rodrigohidaka");
	    obj.findElement(By.name("senha")).sendKeys("123456");
	    //Menu
	   // obj.findElement(By.cssSelector("input")).submit();
	    obj.findElement(By.linkText("Menu")).click();
	    obj.findElement(By.id("buscarMenuTxt")).sendKeys("Gerenciamento do SAC");
	    obj.findElement(By.linkText("Gerenciamento do SAC")).click();
	
	    //Preenchimento na pagina do Gerenciamento do SAC
	    System.out.println(m.getCondutor());
	    obj.switchTo().frame("app");
	    obj.switchTo().frame("main_ifr");
	    obj.findElement(By.id("Contato")).sendKeys( m.getCondutor());
	    //obj.findElement(By.name("Solicitante")).sendKeys(m.getSolctante());
	    obj.findElement(By.name("Email")).sendKeys(m.getEmail());
	    obj.findElement(By.name("Telefone")).sendKeys(m.getTelefone());
	    obj.findElement(By.name("AreaID")).sendKeys(m.getArea());
	    obj.findElement(By.name("OrigemID")).sendKeys(m.getOrigemdoContato());
	    obj.findElement(By.name("Motivos")).sendKeys(m.getMotivos());
	    obj.findElement(By.name("Submotivos")).sendKeys(m.getSubMotivos());
	    obj.findElement(By.name("SituacaoID")).sendKeys(m.getCanalAtendimento());
	    //obj.findElement(By.name("DataSolicitacao")).sendKeys(m.get());
	    obj.findElement(By.name("CategoriaID")).sendKeys(m.getCategoria());
	    obj.findElement(By.name("PrioridadeID")).sendKeys(m.getPrioridade());
	    obj.findElement(By.name("id_hugme")).sendKeys(m.getIDHugme());
	
	    obj.findElement(By.id("ok")).submit();
	
	}
}
