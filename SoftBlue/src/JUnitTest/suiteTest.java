package JUnitTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
//indica��o que esta � a classe suite para testes
//em grupo e que deve ser executada como
@RunWith(Suite.class)
//como iremos trabalhar com arrays de classes, devemos 
//inserir como par�metros valores entre {}
@SuiteClasses({ArrayUtilTest.class, JUnitTestString.class})
public class suiteTest {}
