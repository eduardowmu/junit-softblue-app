package out.production.test;
public class massa {

    private String Condutor;
    private String Solctante;
    private String Email;
    private String Telefone;
    private String Area;
    private String OrigemdoContato;
    private String Motivos;
    private String SubMotivos;
    private String CanalAtendimento;
    //private DateTime DataSolicitacao;
    //private DateTime DataAbertura;
    //private DateTime DataEncerramento;
    private String Categoria;
    private String Prioridade;
    //private String Avaliacao;
    //private String ValorReembolsado;
    //private String Status;
    private String IDHugme;


    public String getCondutor() {
        return Condutor;
    }

    public void setCondutor(String condutor) {
        Condutor = condutor;
    }

    public String getSolctante() {
        return Solctante;
    }

    public void setSolctante(String solctante) {
        Solctante = solctante;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getTelefone() {
        return Telefone;
    }

    public void setTelefone(String telefone) {
        Telefone = telefone;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getOrigemdoContato() {
        return OrigemdoContato;
    }

    public void setOrigemdoContato(String origemdoContato) {
        OrigemdoContato = origemdoContato;
    }

    public String getMotivos() {
        return Motivos;
    }

    public void setMotivos(String motivos) {
        Motivos = motivos;
    }

    public String getSubMotivos() {
        return SubMotivos;
    }

    public void setSubMotivos(String subMotivos) {
        SubMotivos = subMotivos;
    }

    public String getCanalAtendimento() {
        return CanalAtendimento;
    }

    public void setCanalAtendimento(String canalAtendimento) {
        CanalAtendimento = canalAtendimento;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String categoria) {
        Categoria = categoria;
    }

    public String getPrioridade() {
        return Prioridade;
    }

    public void setPrioridade(String prioridade) {
        Prioridade = prioridade;
    }

    public String getIDHugme() {
        return IDHugme;
    }

    public void setIDHugme(String IDHugme) {
        this.IDHugme = IDHugme;
    }
}