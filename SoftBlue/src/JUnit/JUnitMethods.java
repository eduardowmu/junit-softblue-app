package JUnit;

public class JUnitMethods 
{	public static boolean isEmpity(String s)
	{	if(s == null)	return true;
		
		s = s.trim();
		return s.length() == 0;
	}
	
	public static String reverse(String s)
	{	if(s == null)	return s;
		
		StringBuilder sb = new StringBuilder();
		//inverte a �rdem dos caracteres da string
		sb.append(s).reverse();
		return sb.toString();
	}
}