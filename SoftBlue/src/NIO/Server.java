package NIO;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
public class Server
{	private int gate;//server's gate
	//selector como atributo
	private Selector selector;
	//buffer de tipo byte
	private ByteBuffer buffer;
	//mensagem que cliente enviar�
	private String msg;
	/*constructor, which we must define, as
	 parameter, the gate*/
	public Server(int gate) throws IOException
	{	this.gate = gate;
		this.selector = Selector.open();
		//buffer de 1kB
		this.buffer = ByteBuffer.allocate(1024);
	}
	
	//method responsable to start server 
	public void start() throws IOException
	{	//server must wait connections. ServerScoketChannel
		//is the channel we use in NIO API in server side,
		//wich must wait for connections, across this channel
		ServerSocketChannel ssc = ServerSocketChannel.open();
		
		//non blocked channel, it's an assinchronous, it means
		//while this channel is avaiable, it won't be blocked
		//its pattern value is TRUE, so we must define it as FALSE
		ssc.configureBlocking(false);
		
		//next step is to link this channel with gate where we
		//wanna open our server. Method socket() retorns a
		//ServerSocket type, witch has a bind() method, a
		//necessary operation to link our server to gate
		ssc.socket().bind(new InetSocketAddress(gate));
		
		//registry this ServerSocketChannel to be warmed
		//about some connection attemptive. Everytime
		//this channel receive a connection from something
		//this selector will know about and we can make
		//some actions on this event
		ssc.register(selector, SelectionKey.OP_ACCEPT);
		
		//an selector stays in a looping, wich makes 
		//checks all events
		while(true)//m�todo que bloqueia at� o momento que 
		{	selector.select();//acontecer algum evento no canal
							  //e quando acontecer, desbloqueia o canal
			//Itera��o do tipo Chave de sele��o. o m�todo selectedKeys retorna
			//a lista de chaves selecionadas, transformadas em eventos de acordo
			//com o que ocorreu com o canal
			Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
			//looping por todas essas chaves
			while(iter.hasNext())
			{	SelectionKey key = iter.next();
				//temos interesse em eventos do tipo Accept. Se for
				if(key.isAcceptable())
				{connect(key);}//faz a conex�o atrav�s da chave
				//se esta chave � respons�vel para o envio de dados
				//ao servidor
				else if(key.isReadable())
				{read(key);}//outro m�todo para leitura de dados
				//se for para escrita de dados, realize a escrita
				//de dados da forma invertida
				else if(key.isWritable()) write(key);
				
				//muito importante que a itera��o seja fechada no final
				//pois da proxima vez que for chamada a chave, essa itera��o 
				//ainda permanecer� em execu��o e ficar� na espera
				iter.remove();
			}
		}
	}
	private void connect(SelectionKey key) throws IOException
	{	//preparar o socket server para ler as informa��es do cliente
		ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
		//chamando o canal que fica entre o cliente e servidor, onde
		//ser�o retornados os dados. Este Accept n�o bloquear� o fluxo
		//de dados
		SocketChannel sc = ssc.accept();
		//chamando o canal assincrono, evita que ocorra o bloqueio de
		//opera��o R/W.
		sc.configureBlocking(false);
		//ler a informa��es do canal e para isso devemos registrar
		//o selector neste canal para que ele avise o servidor no
		//momento em que um dado estiver disponivel para leitura
		sc.register(selector, SelectionKey.OP_READ);
		
	}//antes de que os dados sejam enviados do cliente para o
	//servidor, estes dever�o ser lidos
	private void read(SelectionKey key) throws IOException
	{	SocketChannel sc = (SocketChannel) key.channel();
		//limpando o buffer para possibilitar a leitura
		buffer.clear();
		//leitura dos dados no buffer
		sc.read(buffer);
		//convertendo o modo de escrita do buffer, que � por padr�o,
		//em modo de leitura
		buffer.flip();
		//convertendo dados de bytebuffer para String,
		//atrav�s de um array de bytes. o m�todo
		//remaining retorna um inteiro, que indica
		//qtos bytes temos dentro do buffer para ser
		//lido
		byte[] bytes = new byte[buffer.remaining()];
		//extraindo informa��es dentro do ByteBuffer e coloc�-los
		//dentro do array de bytes
		buffer.get(bytes);
		//iniciando a String, que recebe os dados em bytes
		msg = new String(bytes);
		//registrar o evento de escrita para que sejam escritos
		//dados lidos no buffer
		sc.register(selector, SelectionKey.OP_WRITE);
	}//m�todo de escrita de dados, de forma invertida
	private void write(SelectionKey key) throws IOException
	{	//refer�ncia ao canal que ser� escrita
		SocketChannel sc = (SocketChannel) key.channel();
		//inverter a String da msg. O m�todo toString()
		//foi usado para converter o StringBuilder para String
		msg = new StringBuilder(msg).reverse().toString();
		//convertendo a String em bytes
		byte[] bytes = msg.getBytes();
		//limpando o buffer
		buffer.clear();
		//insere os bytes no buffer
		buffer.put(bytes);
		/*Como agora iremos passar o canal para o buffer, fazemos um flip para
		 transformarmos o buffer em modo de leitura*/
		buffer.flip();
		/*e no SocketChannel iremos escrever os dados que est�o no buffer
		 no canal de sa�da, que no caso � o canal do cliente*/
		sc.write(buffer);
		/*supondo que o cliente p�ra de receber as mensagens*/
		sc.register(selector, 0);
	}//main
	public static void main(String[] args) throws IOException
	{	//instanciando a pr�pria classe
		Server server = new Server(4646);
		//in�ciar o servidor
		server.start();
		
	}
}
