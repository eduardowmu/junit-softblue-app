package NIO;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

public class Client 
{	private String host;
	private int port;
	private String msg;
	private Selector selector;
	/*flag que indica se o looping de envio ao servidor deve terminar ou n�o
	 pois quando uma msg � enviado ao servidor e este retornar uma resposta,
	 o cliente tem que terminar*/
	private boolean finished;
	private ByteBuffer buffer;
	//ser� a mesma mensagem escrita, s� que invertida
	private String msg2;
	
	//constructor
	public Client(String host, int port) throws Exception
	{	this.host = host;
		this.port = port;
		this.selector = Selector.open();
		this.finished = false;
		//um buffer de 1kB
		this.buffer = ByteBuffer.allocate(1024);
	}
	//m�todo de envio de mensagens, que ir� enviar uma mensagem e
	//retornar uma mensagem invertida
	public String sendMsg(String msg) throws IOException
	{	this.msg = msg;
		//precisamos inicialmente nos conectar com o servidor, portanto
		//primeira coisa a fazer � abrir um SocketChannel. Aqui n�o temos
		//a figura do ServerSocketChannel pois estamos do lado do cliente
		SocketChannel sc = SocketChannel.open();
		//como iremos definir um IO ass�ncrono, iremos dizer que o bloqueio de
		//configura��o � falso, ou seja, n�o haver� bloqueio nas opera��es de IO 
		sc.configureBlocking(false);
		//A primeira coisa que um cliente deve fazer � se conectar, e portanto, este
		//deve fornecer informa��es de onde quer se conectar, que ser� no host e porta
		//fornecida
		sc.connect(new InetSocketAddress(host, port));
		/*o m�todo connect, como o canal � ass�ncrono, este n�o vai retornar apenas
		 depois que a conex�o for feita, iremos chamar conex�o e o m�todo ir� retornar
		 imediatamente, mesmo que a conex�o n�o tenha sido totalmente estabelecida. 
		 Portanto devemos fazer nosso c�dugo observar pelo evento de conex�o do canal
		 pra quando a conex�o estiver pronta para ser realizada para depois realmente
		 efetua-la. Iremos ent�o registrar*/
		sc.register(selector, SelectionKey.OP_CONNECT);
		//looping do selector que vai ficar aguadando a ocorr�ncia de eventos no canal,
		//com base em uma flag
		while (!finished)
		{	//dentro do looping, tudo funciona quase que do mesmo jeito que no servidor
			/*1-chamar o m�todo select, que ir� ficar bloqueado, at� que ocorra um
			evento que tenhamos interesse*/ 
			selector.select();
			/*2-No momento que ocorrer o evento, a primeira coisa que faz � extrair
			 ent�o, de dentro do selector as chaves seletoras e as iterar sobre eles*/
			Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
			/*3-looping sobre as chaves*/
			while(iter.hasNext())//pega a proxima chave da itera��o
			{	SelectionKey key = iter.next();
				/*teste de conex�o da chave, caso esta chave seja conect�vel. Quando
				chamamos este m�todo e este faz um retorno, � poss�vel que a conex�o
				n�o tenha sido totalme te estabelecida, pois esta conex�o � ass�ncrono
				quando chamamos num canal, cujo ConfigureBlocking est� definido para
				false. No momento em que chamamos o select no looping de cima, se a
				conex�o ainda n�o terminou, ir� gerar um evento de que neste momento
				pode ser terminada e isso ir� fazer com que nosso m�todo connect()
				seja chamado*/
				if(key.isConnectable())	connect(key);//realiza conex�o
				//tamb�m devemos estar preparado para quando ocorrer o evento
				//de que o canal est� pronto para ter dados escritos nele. Caso
				//positivo, chama o m�todo de escrever
				else if(key.isWritable())	write(key);
				//se existirem dados para serem lidos no canal
				else if(key.isReadable())	read(key);
				//n�o devemos esquecer que sempre que finalizar uma itera��o, devemos
				//remov�-lo
				iter.remove();
			}
		}
		//quando o looping terminar, a mensagem estar� invertida, portanto a mensagem
		//deve ser o msg2.
		return msg2;
	}
	//m�todo de conex�o. 
	public void connect(SelectionKey key) throws IOException
	{	//1-c�digo que termina de estabelecer as conex�o
		SocketChannel sc = (SocketChannel)key.channel();
		//2-fim da conex�o
		sc.finishConnect();
		//a partir de agora iremos poder enviar dados, ou seja, escrever dados
		//no canal, precisamos agora observer o momento que o canal est� pronto para
		//a escrita dos dados. Logo, iremos registrar no SocketChannel o selector
		//para observar eventos do tipo escrita
		sc.register(selector, SelectionKey.OP_WRITE);
	}
	public void write(SelectionKey key) throws IOException
	{	//1- Para escrever dados em um canal, precisamos de um canal, que tem como
		//refer�ncia o SocketChannel
		SocketChannel sc = (SocketChannel)key.channel();
		//2-escrever alguma informa��o dentro do canal, que � a mensagem que estamos
		//querendo enviar pelo m�todo sendMsg(). O pr�ximo passo � extrair os bytes
		//dessa mensagem. O m�todo abaixo nos retorna um array de bytes
		byte[] bytes = msg.getBytes();
		//3-lembrando que devemos SEMPRE ler ou escrever dados dentro de um buffer e
		//NUNCA diretamente dentro de um canal. Para inserir os dados do canal para
		//o buffer. Mas antes de inserir os dados no buffer, � recomend�vel fazer uma
		//limpeza nele antes para garantir que contenha apenas o que for escrito nele.
		buffer.clear();
		buffer.put(bytes);
		//4-depois de escrever tudo no buffer, devemos mudar o buffer para o modo de 
		//leitura, para que seja poss�vel ler seus dados escritos e em seguida, envi�-los
		//ao servidor
		buffer.flip();
		//5- feita altera��o para o modo de leitura, podemos enviar os dados ao servidor
		sc.write(buffer);
		/*6-ap�s enviar os dados, devemos aguardar o servidor retornar a msg invertida
		e para isso devemos aguardar eventos de leitura de canal, ou seja, ficar esperando
		at� que o canal esteja pronto para ler, ou seja, que os dados estejam dispon�veis
		para ler. Portanto iremos registrar a opera��o de leitura*/
		sc.register(selector, SelectionKey.OP_READ);
	}
	
	public void read(SelectionKey key) throws IOException
	{	//1-assim como nos outros m�todos, precisamos de um canal
		SocketChannel sc = (SocketChannel)key.channel();
		//2-precisamos de um buffer para fazer a leitura, podendo ser o mesmo usado
		//anteriormente e reaproveit�-lo
		buffer.clear();
		//3-chama o m�todo de leitura do canal, que far� com que os dados vindo do servidor
		//sejam copiados e enviados para dentro do buffer
		sc.read(buffer);
		//4-Agora o buffer ir� conter os dados e no caso � a string que foi enviado, por�m
		//invertida. Precisamos ent�o extrair os dados do buffer. Portanto, colocamos o buffer
		//no modo de leitura
		buffer.flip();
		//5-A leitura dos dados ser�o feitos em bytes. O m�todo � o get(PAR�METRO), cujo 
		//par�metro � um array de bytes para que copie os dados que est�o dentro do buffer
		//para o array de bytes, de tamanho que est�o dispon�veis para leitura
		byte[] bytes = new byte[buffer.remaining()];
		//6-assim copiamos o array de bytes para dentro do buffer
 		buffer.get(bytes);
 		//7-Tendo o array de bytes, podemos construir uma string, que ter� como par�metro
 		//o array de bytes
 		msg2 = new String(bytes);
 		//8-como ja terminamos nossa comunica��o do cliente-servidor e o cliente ir� terminar
 		//portanto primeiro fechamos o canal
 		sc.close();
 		//9-assim podemos teminar o looping
 		finished = true;
	}
}
